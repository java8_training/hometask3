import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;

public class Main {
    static List<Product> productsList = new ArrayList<>();

    public static void main(String[] args) {
        CommonFunctions commonFunctions = new CommonFunctions();
        productsList = commonFunctions.setProductsList();
        Predicate<Product> predicatePriceCheck = product -> product.productPrice > 1000;
        Predicate<Product> predicateCategoryCheck = product -> product.productCategory.equalsIgnoreCase("Electronics");

        //FunctionalInterface 1 - calculate the cost of all products in a given list of products
        Function<List<Product>, Double> calculatePriceOfAllProducts = products -> {
            double totalCost = 0;
            for (Product p:products) {
                totalCost += p.productPrice;
            }
            return totalCost;
        };
        System.out.println("Total price of all Products : "+calculatePriceOfAllProducts.apply(productsList));

        //FunctionalInterface 2 - calculate the cost of all products whose prices is > 1000/-
        Function<List<Product>, Double> calculatePriceOfAllProductsAbove1000 = products -> {
            double totalCost = 0;
            for (Product p:products) {
                if(predicatePriceCheck.test(p))
                    totalCost += p.productPrice;
            }
            return totalCost;
        };
        System.out.println("Total price of all Products  (Above 1000): "+calculatePriceOfAllProductsAbove1000.apply(productsList));

        //FunctionalInterface 3 - calculate the cost of all electronic products
        Function<List<Product>, Double> calculatePriceOfAllElectronics = products -> {
            double totalCost = 0;
            for (Product p:products) {
                if(predicateCategoryCheck.test(p))
                    totalCost += p.productPrice;
            }
            return totalCost;
        };
        System.out.println("Total price of all Products (Electronics): "+calculatePriceOfAllElectronics.apply(productsList));

        //FunctionalInterface 3 - calculate the cost of all electronic products
        Function<List<Product>, Double> calculatePriceOfAllElectronicsAbove1000 = products -> {
            double totalCost = 0;
            for (Product p:products) {
                if(predicateCategoryCheck.and(predicatePriceCheck).test(p))
                    totalCost += p.productPrice;
            }
            return totalCost;
        };
        System.out.println("Total price of all Products (Electronics, Above 1000): "+calculatePriceOfAllElectronicsAbove1000.apply(productsList));
    }
}