import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CommonFunctions {
    public List<Product> setProductsList() {
        List<Product> productsList = new ArrayList<>();
        productsList.add(new Product("PS5", 40000, "Electronics", "New"));
        productsList.add(new Product("XBox Series S", 35000, "Electronics", "Used"));
        productsList.add(new Product("iPhone", 80000, "Mobile", "New"));
        productsList.add(new Product("Lucky Luke", 100, "Comics", "New"));
        productsList.add(new Product("Charger", 50, "Electronics", "New"));
        productsList.add(new Product("Alexa", 1000, "Electronics", "New"));
        return productsList;
    }
}
