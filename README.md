# HomeTask3

# Description

# Pre Conditions
- Define a Product class with name, price, category, grade

# Functional Interface

1. Write a function to calculate the cost of all products in a given list of products.

2. Write a function to calculate the cost of all products whose prices is > 1000/- in the given list of products.

3. Write a function to calculate the cost of all electronic products in the given list of products.

4. Write a function to get all the products whose price is is > 1000/- and belongs to electronic category.


# JAVA Classes
Product and Response - As mentioned in the precondition
CommonFunctions - Class to initialise list of Products 
Main - Function calls to create the list, create the Functional Interface and print the results